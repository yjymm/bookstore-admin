import Cookies from 'js-cookie'

// 这个是保存，返回，删除在cookie的token

const TokenKey = 'login_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
