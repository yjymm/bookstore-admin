import axios from '@/axios'

// 获取订单列表
export function getOrderList(getOrderForm) {
  return axios.get(`/order/page/${getOrderForm.current}/${getOrderForm.size}`)
}

export function getOrderById(oid) {
  return axios.get(`/order/orderDetail/${oid}`)
}
export function updateStatus(orderForm) {
  return axios({
    method: 'put',
    url: '/order/update/status',
    data: orderForm
  })
}

export function getUserOrderList(uid) {
  return axios.get(`/order/getOrderList/${uid}`)
}

export function timeFilter(orderQo, page) {
  const data = {
    ...orderQo,
    current: page.current,
    size: page.size
  }
  // console.log(data)
  return axios({
    method: 'post',
    url: '/order/getTimeList',
    data
  })
}
