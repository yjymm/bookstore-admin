import axios from '@/axios'

export function delBookImgs(bookId, imgIds) {
  return axios({
    method: 'post',
    url: `/public/bookImg/deleteImgList/${bookId}`,
    data: imgIds
  })
}

export function addBookImgNames(bookId, bookList) {
  return axios({
    method: 'post',
    url: `/public/bookImg/addImgs/${bookId}`,
    data: bookList
  })
}

// 获取图书列表
export function getBookList(getBookForm) {
  return axios.get(`/public/book/getList/${getBookForm.current}/${getBookForm.size}/${getBookForm.orderType}`)
}

// 根据id获取图书
export function getBookDetailById(id) {
  return axios.get(`/public/book/getBookById/${id}`)
}

// 查询书籍
export function searchBooks(searchBookForm, searchType, str) {
  return axios.get(`public/book/search/${searchBookForm.current}/${searchBookForm.size}/${searchType}/${searchBookForm.orderType}`, {
    params: {
      str: str
    }
  })
}

export function addNewBook(payload) {
  return axios({
    method: 'post',
    url: '/book/addBooks',
    data: payload
  })
}

export function updateBook(form) {
  return axios({
    method: 'post',
    url: '/book/updateBook',
    data: form
  })
}

export function deleteBookById(id) {
  return axios({
    method: 'post',
    url: `/book/deleteBook/${id}`,
    data: id
  })
}

// type =1 批量删除 =2批量上架 =3批量下架
export function handleSelectedBooks(type, arr) {
  return axios({
    method: 'post',
    url: `/book/handleSelectedBooks/${type}`,
    data: arr
  })
}

export function handleGrounding(payload) {
  return axios({
    method: 'post',
    url: '/book/handleGrounding',
    data: payload
  })
}

// 根据图书类别
export function getCategory(getCategoryForm) {
  return axios.get(`public/category/getList/${getCategoryForm.current}/${getCategoryForm.size}/${getCategoryForm.orderType}`)
}

export function getCategoryDetailById(id) {
  return axios.get(`public/category/getCategoryById/${id}`)
}

export function updateCategory(form) {
  return axios({
    method: 'post',
    url: '/category/updateCategory',
    data: form
  })
}

export function deleteCategory(cId) {
  return axios.post(`category/deleteCategory/${cId}`)
}

export function addCategory(addForm) {
  return axios({
    method: 'post',
    url: '/category/addCategory',
    data: addForm
  })
}

export function getSchedule(data) {
  return axios({
    method: 'get',
    url: '/schedule',
    params: data
  })
}

export function addSchedule(data) {
  return axios({
    method: 'post',
    url: '/schedule',
    data: data
  })
}

export function delSchedule(data) {
  return axios({
    method: 'delete',
    url: '/schedule',
    params: data
  })
}

export function startSchedule(data) {
  return axios({
    method: 'put',
    url: '/schedule/status',
    params: data
  })
}

export function getBookNames() {
  return axios({
    method: 'get',
    url: '/public/book/getName'
  })
}

export function getBooksByCid(getBookByCategoryForm) {
  return axios.get(`/public/book/getBooksByCid/${getBookByCategoryForm.current}/${getBookByCategoryForm.size}/${getBookByCategoryForm.cId}/${getBookByCategoryForm.orderType}`)
}
