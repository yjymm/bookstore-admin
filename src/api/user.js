import axios from '@/axios'
// import qs from 'qs'

export function login(data) {
  // const data = Qs.stringify(loginForm)
  // eslint-disable-next-line object-curly-spacing
  return axios({
    url: '/auth/admin/login',
    method: 'post',
    params: data
  })
}

export function update(adminQo) {
  return axios({
    method: 'put',
    url: '/admin',
    data: adminQo
  })
}

// 获取登陆admin的信息
export function getInfo() {
  return axios.get('/admin/getInfo')
}

// 获取admin列表
export function getAdminList() {
  return axios.get('/admin/list')
}

// user列表
export function getUserList() {
  return axios.get('/admin/all')
}

export function setUserStatus(userQo) {
  return axios({
    method: 'put',
    url: '/user',
    data: userQo
  })
}

export function setAdminStatus(adminQo) {
  return axios({
    method: 'put',
    url: '/admin',
    data: adminQo
  })
}

export function addAdmin(adminQo) {
  return axios({
    method: 'post',
    url: '/admin/register',
    data: adminQo
  })
}

// 获取当前登陆的管理员的信息
export function getAdminInfo() {
  return axios.get('/admin/getInfo')
}

export function getRoleList() {
  return axios.get('/role/list')
}

// export function addAdmin(form) {
//   return axios({
//     method: 'post',
//     url: '/admin/register',
//     data: form
//   })
// }
