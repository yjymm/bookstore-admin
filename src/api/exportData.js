import axios from '@/axios'

// 导出用户信息
export function downloadUser() {
  return axios({
    method: 'get',
    url: `/api/export/downloadUser`
  })
}

// 导出订单信息
export function downloadOrder() {
  return axios({
    method: 'get',
    url: `/api/export/downloadOrder`
  })
}

// 导出图书信息
export function downloadBook() {
  return axios({
    method: 'post',
    url: `/api/export/downloadBook`,
    responseType: 'blob'
  })
}

export function outExportExcel(tHeader = [], filterVal = [], listData = [], exportName = new Date().getTime()) {
  require.ensure([], () => {
    // 注意这个Export2Excel路径
    const { export_json_to_excel } = require('@/vendor/Export2Excel')
    const data = formatJson(filterVal, listData)
    export_json_to_excel(tHeader, data, exportName)
  })
}

function formatJson(filterVal, jsonData) {
  return jsonData.map(v => filterVal.map(j => v[j]))
}
