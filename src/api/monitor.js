import axios from '@/axios'

// 获取系统信息
export function getSystemInfo() {
  return axios.get(`/api/monitor`)
}
