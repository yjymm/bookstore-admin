import axios from '@/axios'

// 获取商品信息
export function getGoodsInfo(start, end) {
  return axios({
    method: 'post',
    url: `/api/data/getAllGoodsData`,
    data: {
      'start': start,
      'end': end
    }
  })
}

// 获取订单信息
export function getOrderInfo(start, end) {
  // return axios.post(`/api/data/getAllOrderData`)
  return axios({
    method: 'post',
    url: `/api/data/getAllOrderData`,
    data: {
      'start': start,
      'end': end
    }
  })
}

// 获取首页信息
export function getDashBoardInfo() {
  return axios.post(`/api/data/getDashBoardData`)
}
