import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
// import componentsRouter from './modules/components'
// import chartsRouter from './modules/charts'
// import tableRouter from './modules/table'
// import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },

  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user', noCache: true, affix: true }
      }
    ]
  },

  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  }

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    // 订单管理模块
    path: '/order',
    component: Layout,
    redirect: '/order/index',
    name: 'ordermanage',
    meta: {
      title: '订单管理',
      icon: 'el-icon-tickets',
      roles: ['ROLE_ADMIN', 'ROLE_ORDER']
    },
    children: [
      {
        // 订单列表，展示所有订单
        path: 'index',
        name: 'Order',
        component: () => import('@/views/order/index'),
        meta: {
          title: '订单列表',
          icon: 'el-icon-tickets',
          roles: ['ROLE_ADMIN', 'ROLE_ORDER']
        }
      }, {
        // 订单详情
        path: 'detail/:oid',
        name: 'OrderDetail',
        hidden: true,
        component: () => import('@/views/order/detail'),
        meta: {
          title: '订单详情',
          icon: 'el-icon-tickets',
          roles: ['ROLE_ADMIN', 'ROLE_ORDER']
        }
      }, {
        // 展示所有用户列表，然后查询某个用户的所有订单
        path: 'user',
        name: 'OrderUser',
        component: () => import('@/views/order/user'),
        meta: {
          title: '用户列表',
          icon: 'el-icon-user',
          roles: ['ROLE_ADMIN', 'ROLE_ORDER']
        }
      }, {
        // 展示所有用户列表，然后查询某个用户的所有订单
        path: 'userList/:uid',
        name: 'OrderUser',
        hidden: true,
        component: () => import('@/views/order/userOrderDetail'),
        meta: {
          title: '用户订单列表',
          icon: 'el-icon-tickets',
          roles: ['ROLE_ADMIN', 'ROLE_ORDER']
        }
      }]
  },

  {
    // 图书管理模块
    path: '/book',
    component: Layout,
    name: 'bookmanage',
    redirect: '/book/index',
    meta: {
      title: '图书管理',
      icon: 'el-icon-s-goods',
      roles: ['ROLE_ADMIN', 'ROLE_BOOK']
    },

    children: [
      {
        path: 'index',
        name: 'BookSelect',
        component: () => import('@/views/book/index'),
        meta: {
          title: '图书查询',
          icon: 'el-icon-search',
          roles: ['ROLE_ADMIN', 'ROLE_BOOK']
        }
      }, {
        // 发布秒杀商品
        path: 'seckill',
        name: 'BookSeckill',
        component: () => import('@/views/book/seckill'),
        meta: {
          title: '图书活动管理',
          icon: 'el-icon-mouse',
          roles: ['ROLE_ADMIN', 'ROLE_BOOK']
        }
      }, {
        // 查看图书详情
        path: 'detail/:id',
        name: 'BookDetail',
        component: () => import('@/views/book/detail'),
        hidden: true,
        meta: {
          title: '图书详情',
          roles: ['ROLE_ADMIN', 'ROLE_BOOK']
        }
      }, {
        path: 'add',
        name: 'BookAdd',
        component: () => import('@/views/book/add'),
        meta: {
          title: '图书添加',
          icon: 'el-icon-edit',
          roles: ['ROLE_ADMIN', 'ROLE_BOOK']
        }
      }, {
        path: 'category',
        name: 'category',
        component: () => import('@/views/book/category'),
        meta: {
          title: '种类管理',
          icon: 'el-icon-folder-opened',
          roles: ['ROLE_ADMIN', 'ROLE_BOOK']
        }
      }]
  },
  {
    // 管理员管理模块
    path: '/admin',
    component: Layout,
    redirect: '/admin/index',
    meta: {
      title: '管理模块',
      icon: 'el-icon-setting',
      roles: ['ROLE_ADMIN']
    },
    name: 'adminmanage',
    children: [
      {
        path: 'index',
        name: 'Admin',
        component: () => import('@/views/admin/index'),
        meta: {
          title: '后台管理',
          icon: 'el-icon-setting',
          roles: ['ROLE_ADMIN']
        }
      }, {
        path: 'user',
        name: 'UserManage',
        component: () => import('@/views/admin/user'),
        meta: {
          title: '用户管理',
          icon: 'el-icon-user',
          roles: ['ROLE_ADMIN']
        }
      }
    ]

  },
  {
    // 数据分析模块
    path: '/data',
    component: Layout,
    redirect: '/data/index',
    name: 'datamanage',
    meta: {
      title: '数据分析',
      icon: 'el-icon-data-board',
      roles: ['ROLE_ADMIN', 'ROLE_DATA']
    },
    children: [
      {
        path: 'monitor',
        component: () => import('@/views/data/monitor'),
        name: '系统监控',
        meta: {
          title: '系统监控',
          icon: 'el-icon-s-tools',
          roles: ['ROLE_ADMIN', 'ROLE_DATA']
        }
      },
      // {
      //   path: 'netflow',
      //   component: () => import('@/views/data/netflow'),
      //   name: '网站流量',
      //   meta: {
      //     title: '网站流量',
      //     icon: 'el-icon-data-board',
      //     roles: ['ROLE_ADMIN', 'ROLE_DATA']
      //   }
      // },
      {
        path: 'goods',
        component: () => import('@/views/data/goods'),
        name: '商品指标',
        meta: {
          title: '商品指标',
          icon: 'el-icon-notebook-1',
          roles: ['ROLE_ADMIN', 'ROLE_DATA']
        }
      },
      {
        path: 'sales',
        component: () => import('@/views/data/sales'),
        name: '销售指标',
        meta: {
          title: '销售指标',
          icon: 'el-icon-sell',
          roles: ['ROLE_ADMIN', 'ROLE_DATA']
        }
      }
    ]

  }
  // 404 page must be placed at the end !!!
  // {
  //   path: '*', redirect: '/404', hidden: true
  // }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
