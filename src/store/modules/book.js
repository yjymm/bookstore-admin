import { getCategory, addNewBook, getSchedule } from '@/utils/book'

const state = {
  categoryList: ''
}

const mutations = {
  SET_CATEGORYLIST: (state, list) => {
    state.categoryList = list
  }
}

const actions = {

  getCategory({ commit }) {
    return new Promise((resolve, reject) => {
      getCategory().then(res => {
        commit('SET_CATEGORYLIST', res.object)
        resolve(res.object)
      }).catch(error => {
        reject(error)
      })
    })
  },

  addBook({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addNewBook(payload).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  },

  getSchedule({ commit }, data) {
    return new Promise((resolve, reject) => {
      getSchedule(data).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
